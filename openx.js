(function ($) {
  Drupal.behaviors.openx = {
    attach: function (context) {
      document.write = document.writeln = function(html) {
        $('#openxadscontainer').append(html);
      };

      if (typeof(OA_show) != 'undefined') {
        $('div#openxads:not(.openx-class-processed)',context).each(function () {
          $(this).addClass('openx-class-processed');

          OA_show($(this).html());

          // If the output contains external script alternate method of inserting required.
          var s = OA_output[$(this).html()];

          var match = s.match(/<script.*src="/g);
          if (match) {
            var div = document.createElement('div');
            div.innerHTML = s;
            this.parentNode.insertBefore(div, this.nextSibling);
          }
          else {
            $(this).after($('#openxadscontainer').html());
          }
          $('#openxadscontainer').empty();
        });
      }

    }
  };
})(jQuery);

